This basic CRUD operation APIs are created in Java Spring Boot.

4 APIs are created inside the project: 
/User/InsertUser (Post Method) 
/User/GetUser (Get Method) 
/User/UpdateUser (Patch Method) 
/User/DeleteByUserId (Delete Method)

Softwares/Techs Used: 
Java 1.8 
Spring Boot 
MySQL/ MySQL Workbranch (DB) 
iBatis (ORM) 
Postman (Request handling) 
Swagger(UI representation of APIs) 
Eclipse IDE Tomcat (host Server)

Database Table Query: 
CREATE TABLE ansh.tbuser ( fdUserId VARCHAR(20) NOT NULL, fdUserName VARCHAR(45) NULL, 
fdUserContact VARCHAR(45) NULL, fdUserCreationDate VARCHAR(45) NULL, PRIMARY KEY (fdUserId));

Attachments are already added, i.e, request and response. 
https://bitbucket.org/anshhora97/usrcrudprj/src/master/src/main/resources/static/screenshots/