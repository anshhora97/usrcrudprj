package com.rs.user.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.rs.user.model.UserModel;
import com.rs.user.response.UserResponse;
import com.rs.user.service.UserService;

@RestController
@RequestMapping("/User")
public class UserController {

	@Autowired
	private UserService userservice;

	UserResponse resp = new UserResponse();

	@PostMapping("/InsertUser")
	public UserResponse insertUser(@RequestBody UserModel usermodel) {
		resp = userservice.insertOrder(usermodel);
		return resp;
	}

	@PatchMapping("/UpdateUser")
	public UserResponse updateUser(@RequestParam String userId, @RequestBody UserModel usermodel) {
		resp = userservice.updateUser(userId, usermodel);
		return resp;
	}

	@GetMapping("/GetUser")
	public UserResponse getOrder(@RequestParam String userId) {
		resp = userservice.getUserbyUserId(userId);
		return resp;
	}

	@DeleteMapping("/DeleteByUserId")
	public UserResponse deleteByUserId(@RequestParam String userId) {
		resp = userservice.deleteUser(userId);
		return resp;
	}
	
//	
}
