package com.rs.user.service;

import com.rs.user.model.UserModel;
import com.rs.user.response.UserResponse;

public interface UserService {

	UserResponse insertOrder(UserModel ordermodel);

	UserResponse deleteUser(String orderId);

	UserResponse getUserbyUserId(String userId);

	UserResponse updateUser(String userId, UserModel usermodel);

}
