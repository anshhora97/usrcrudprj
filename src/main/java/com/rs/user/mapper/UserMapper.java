package com.rs.user.mapper;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.rs.user.model.UserModel;

@Mapper
public interface UserMapper {

	@Delete("Delete from tbuser where fdUserId=#{userId};")
	void deleteUserbyUserId(@Param("userId") String userId);

	@Insert("INSERT INTO tbuser(fdUserId,fdUserName,fdUserContact,fdUserCreationDate)"
			+ "VALUES (#{userId},#{userName},#{userContact},#{userCreationDate})")
	void saveUserDetails(UserModel model);

	@Select("select fdUserId as 'userId',fdUserName as 'userName',fdUserContact as 'userContact', fdUserCreationDate as 'userCreationDate' from tbuser where fdUserId=#{userId}")
	UserModel getUserbyUserId(@Param("userId") String userId);

	@Update("Update tbuser set fdUserName=#{userName}, fdUserContact=#{userContact} where fdUserId=#{userId} ")
	void updateUser(UserModel usermodel);

}
