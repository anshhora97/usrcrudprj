package com.rs.user.model;

import lombok.Data;

@Data
public class UserModel {

	private String userId;
	private String userName;
	private String userContact;
	private String userCreationDate;

}