package com.rs.user.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rs.user.mapper.UserMapper;
import com.rs.user.model.UserModel;
import com.rs.user.response.UserResponse;
import com.rs.user.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserMapper mapper;

	UserResponse resp = new UserResponse();

	@Override
	@Transactional
	public UserResponse insertOrder(UserModel model) {
		try {
			mapper.saveUserDetails(model);
			resp.setResponseStatus(200);
			resp.setResponse("Order Inserted Successfull with Id:" + model.getUserId());
		} catch (Exception e) {
			resp.setResponseStatus(8001);
			resp.setResponse("Order Inserted Failure with Id:" + model.getUserId());
		}
		return resp;
	}

	@Override
	public UserResponse getUserbyUserId(String userId) {
		UserModel model = new UserModel();
		ObjectMapper objmp = new ObjectMapper();
		try {
			model = mapper.getUserbyUserId(userId);
			resp.setResponseStatus(200);
			resp.setResponse("Details Fetced Successfully: " + objmp.writeValueAsString(model));
		} catch (Exception e) {
			resp.setResponseStatus(8001);
			resp.setResponse("Details Fetced Failure");
		}
		return resp;
	}

	@Override
	public UserResponse deleteUser(String userId) {
		try {
			mapper.deleteUserbyUserId(userId);
			resp.setResponse("Successfully Deleted UserId:" + userId);
			resp.setResponseStatus(200);
		} catch (Exception e) {
			resp.setResponse("Failure in Deleting UserId:" + userId);
			resp.setResponseStatus(8001);
		}
		return resp;
	}

	@Override
	public UserResponse updateUser(String userId, UserModel usermodel) {
		resp = deleteUser(userId);
		if (resp.getResponseStatus() == 200) {
			try {
				mapper.updateUser(usermodel);
				resp.setResponse("Successfully Deleted UserId:" + userId);
				resp.setResponseStatus(200);
			} catch (Exception e) {
				resp.setResponse("Failure in Deleting UserId:" + userId);
				resp.setResponseStatus(8001);
			}
		} else {
			resp.setResponse("User Not Present !");
			resp.setResponseStatus(8002);

		}
		return resp;
	}

}
